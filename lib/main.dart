import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_app/ui/home/home.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  MyApp() {
    print("App: MyApp -> MyApp()");
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    print("App: MyApp -> build()");
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
        accentColor: Colors.deepOrange,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text(
            'https://reqres.in/api/users',
            overflow: TextOverflow.visible,
          ),
        ),
        body: Container(
          child: Home(),
        ),
      ),
    );
  }
}
