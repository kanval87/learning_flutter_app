import 'package:flutter_app/models/network/response_getRsources.dart';
import 'package:flutter_app/models/network/response_getuserdetails.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';

import '../models/network/response_getuser.dart';

class NetworkUtils {
  static final NetworkUtils _instance = NetworkUtils._internal();

  factory NetworkUtils() {
    return _instance;
  }

  NetworkUtils._internal();

  static Future<GetUser> getUsers() async {
    final response = await http.get('https://reqres.in/api/users?page=1');

    if (response.statusCode == 200) {
      return GetUser.fromJson(json.decode(response.body));
    } else {
      throw Exception('No Users');
    }
  }

  static Future<GetUserDetails> getUserDetail(userId) async {
    final response =
        await http.get('https://reqres.in/api/users/' + userId.toString());

    if (response.statusCode == 200) {
      return GetUserDetails.fromJson(json.decode(response.body));
    } else {
      throw Exception('No User');
    }
  }

  static Future<ResourcesModel> getResources() async {
    final response = await http.get('https://reqres.in/api/unknown');

    if (response.statusCode == 200) {
      return ResourcesModel.fromJson(json.decode(response.body));
    } else {
      throw Exception('No Resources');
    }
  }
}
