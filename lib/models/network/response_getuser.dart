import './userdata.dart';

class GetUser {
  int page;
  int perPage;
  int total;
  int totalPages;
  List<UserData> data;

  GetUser({this.page, this.perPage, this.total, this.totalPages, this.data});

  GetUser.fromJson(Map<String, dynamic> json) {
    page = json['page'];
    perPage = json['per_page'];
    total = json['total'];
    totalPages = json['total_pages'];
    if (json['data'] != null) {
      data = new List<UserData>();
      json['data'].forEach((v) {
        data.add(new UserData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['page'] = this.page;
    data['per_page'] = this.perPage;
    data['total'] = this.total;
    data['total_pages'] = this.totalPages;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }

  @override
  String toString() {
    return 'GetUser{page: $page, perPage: $perPage, total: $total, totalPages: $totalPages, data: $data}';
  }
}
