import 'package:flutter_app/models/network/userdata.dart';

class GetUserDetails {
  UserData data;

  GetUserDetails({this.data});

  GetUserDetails.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new UserData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }

  @override
  String toString() {
    return 'GetUserDetails{data: $data}';
  }
}
