class ResourceData {
  int id;
  String name;
  int year;
  String color;
  String pantoneValue;
  int index;

  ResourceData({this.id, this.name, this.year, this.color, this.pantoneValue});

  ResourceData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    year = json['year'];
    color = json['color'];
    pantoneValue = json['pantone_value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['year'] = this.year;
    data['color'] = this.color;
    data['pantone_value'] = this.pantoneValue;
    return data;
  }

  @override
  String toString() {
    return 'ResourceData{id: $id, name: $name, year: $year, color: $color, pantoneValue: $pantoneValue, index: $index}';
  }
}
