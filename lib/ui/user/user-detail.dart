import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/models/network/response_getuserdetails.dart';
import 'package:flutter_app/ui/resources/resources-widget.dart';
import 'package:flutter_app/utils/network.dart';
import 'package:url_launcher/url_launcher.dart';

class UserDetail extends StatefulWidget {
  final String userId;

  UserDetail(this.userId);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _UserDetailState();
  }
}

class _UserDetailState extends State<UserDetail> {
  Future<GetUserDetails> _getUserDetails;

  @override
  void initState() {
    super.initState();
    fetchUserDetails(widget.userId);
  }

  @override
  Widget build(BuildContext context) {
    if (userDetail == null) {
      return MaterialApp(
        theme: ThemeData(
          primarySwatch: Colors.deepOrange,
          accentColor: Colors.deepOrange,
        ),
        home: Scaffold(
            appBar: AppBar(
              leading: RaisedButton(
                color: Theme.of(context).primaryColor,
                child: Icon(Icons.arrow_back_ios),
                onPressed: () {
                  if (Navigator.canPop(context)) {
                    Navigator.pop(context);
                  }
                },
              ),
              title: Text('fetching...'),
            ),
            body: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'fetching... ',
                  style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 45,
                  ),
                ),
                CircularProgressIndicator(),
              ],
            )),
      );
    } else
      // TODO: implement build
      return MaterialApp(
        theme: ThemeData(
          primarySwatch: Colors.deepOrange,
          accentColor: Colors.deepOrange,
        ),
        home: Scaffold(
          appBar: AppBar(
            leading: RaisedButton(
              color: Theme.of(context).primaryColor,
              child: Icon(Icons.arrow_back_ios),
              onPressed: () {
                if (Navigator.canPop(context)) {
                  Navigator.pop(context);
                }
              },
            ),
            title: Text(userDetail.data.firstName),
          ),
          body: Container(
            padding: EdgeInsets.all(20),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Column(
                    children: <Widget>[
                      Expanded(
                        child: ListView(
                          children: <Widget>[
                            Image.network(
                              userDetail.data.avatar,
                              fit: BoxFit.cover,
                              width: 1200,
                            ),
                            Text(
                              '${userDetail.data.firstName} ${userDetail.data.lastName}',
                              style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontSize: 45,
                                fontWeight: FontWeight.bold,
                              ),
                              textAlign: TextAlign.center,
                            ),
                            FlatButton.icon(
                              onPressed: () {
                                sendEmail(userDetail.data.email);
                              },
                              icon: Icon(Icons.email),
                              color: Colors.blueGrey,
                              label: Text(
                                userDetail.data.email,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  backgroundColor: Colors.transparent,
                                ),
                              ),
                            ),
                            Divider(
                              color: Theme.of(context).primaryColor,
                              thickness: 1,
                            ),
                            ResourcesWidget(),
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      );
  }

  sendEmail(String email) {
    _launchURL(email, 'Default Subject', 'Default Content');
  }

  _launchURL(String toMailId, String subject, String body) async {
    var url = 'mailto:$toMailId?subject=$subject&body=$body';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  GetUserDetails userDetail;

  fetchUserDetails(String userId) async {
    userDetail = await NetworkUtils.getUserDetail(userId);
    setState(() {});
  }
}
