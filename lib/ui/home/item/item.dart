import 'package:flutter/material.dart';
import 'package:flutter_app/models/network/userdata.dart';

class Item extends StatelessWidget {
  Item(this.element);

  final UserData element;

  @override
  Widget build(BuildContext context) {
    print('App : Item : build()');
    // TODO: implement build
    return Card(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Image.network(
            element.avatar,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Text(
                element.firstName + ' ' + element.lastName,
                textAlign: TextAlign.end,
                style: TextStyle(
                  color: Theme.of(context).textSelectionColor,
                  decorationThickness: .5,
                  fontSize: 30,
                ),
              ),
              Text(element.email),
            ],
          )
        ],
      ),
    );
  }
}
