import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_app/models/network/response_getuser.dart';
import 'package:flutter_app/ui/home/usergrid/userlist.dart';
import 'package:flutter_app/ui/utils/communication-interfaces/bottom-to-top.dart';
import 'package:flutter_app/utils/network.dart';

class Home extends StatefulWidget {
  final String firstItemVal;

  Home({this.firstItemVal = 'Item'});

  @override
  State<StatefulWidget> createState() {
    print('App : Home : createState()');
    return _HomeState();
  }
}

class _HomeState extends State<Home> implements BottomToTop {
  GetUser users;

  bool isNetworkIssue = false;

  @override
  initState() {
    super.initState();
    listForHome();
  }

  Future listForHome() async {
    try {
      this.users = await NetworkUtils.getUsers();
    } catch (e) {
      isNetworkIssue = true;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    if (users != null) {
      return Column(
        children: <Widget>[
          UserList(users, this),
        ],
      );
    } else {
      if (isNetworkIssue) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RaisedButton(
              child: Text('↺ Retry'),
              onPressed: () {
                setState(() {
                  isNetworkIssue = false;
                });
                listForHome();
              },
            )
          ],
        );
      } else {
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'fetching... ',
              style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontWeight: FontWeight.bold,
                fontSize: 45,
              ),
            ),
            CircularProgressIndicator(),
          ],
        );
      }
    }
  }
}
