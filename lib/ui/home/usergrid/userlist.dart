import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_app/models/network/response_getuser.dart';
import 'package:flutter_app/models/network/response_getuserdetails.dart';

import 'package:flutter_app/models/network/userdata.dart';
import 'package:flutter_app/ui/user/user-detail.dart';
import 'package:flutter_app/ui/utils/communication-interfaces/bottom-to-top.dart';
import 'package:flutter_app/utils/network.dart';

class UserList extends StatelessWidget {
  final GetUser _getUser;
  final BottomToTop bottomToTop;

  UserList(this._getUser, this.bottomToTop);

  @override
  Widget build(BuildContext context) {
    if (_getUser == null ||
        _getUser.data == null ||
        _getUser.data.length == 0) {
      return Container();
    } else {
      return Expanded(
        child: ListView.builder(
          itemCount: _getUser.data.length,
          itemBuilder: (context, index) {
            UserData userData = _getUser.data[index];
            return ListTile(
              title: Text(userData.firstName + ' ' + userData.lastName),
              subtitle: Text(userData.email),
              leading: Image.network(userData.avatar),
              onTap: () {
                processUserCardClick(
                  context,
                  userData.id.toString(),
                  // NetworkUtils.getUserDetail(userData.id),
                );
              },
            );
          },
        ),
      );
    }
  }

  /* Future */ processUserCardClick(
      BuildContext context, String userId) /* async */ {
    // GetUserDetails _userDetail = await userDetail;

    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => UserDetail(userId)),
    );

//    bottomToTop.navigateTo(Screens.UserDetail , _userDetail);

//    bottomToTop.msg(_userDetail.toString());

    // print(_userDetail.toString());
    // Scaffold.of(context).showSnackBar(new SnackBar(
    //   content: new Text(_userDetail.data.toString()),
    //   backgroundColor: Theme.of(context).errorColor,
    //   behavior: SnackBarBehavior.floating,
    //   duration: Duration(seconds: 2),
    // ));
  }
}
