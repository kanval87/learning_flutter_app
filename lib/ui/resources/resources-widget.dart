import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/models/network/response_getRsources.dart';
import 'package:flutter_app/utils/network.dart';

import 'list-widget/resource-list-widget.dart';

class ResourcesWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ResourcesWidgetState();
  }
}

class _ResourcesWidgetState extends State<ResourcesWidget> {
  ResourcesModel resourcesModel;

  @override
  void initState() {
    super.initState();
    fetchResouces();
  }

  @override
  Widget build(BuildContext context) {
    if (resourcesModel == null) {
      return Row(
        children: <Widget>[
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Fetching...'),
                CircularProgressIndicator(),
              ],
            ),
          )
        ],
      );
    } else {
      return ResourceListWidget(resourcesModel);
    }
  }

  Future fetchResouces() async {
    resourcesModel = await NetworkUtils.getResources();
    resourcesModel.data.asMap().forEach(
          (i, e) => {e.index = i},
        );
    setState(() {});
  }
}
