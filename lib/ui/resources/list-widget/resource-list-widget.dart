import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_app/models/network/resource-data.dart';
import 'package:flutter_app/models/network/response_getRsources.dart';

class _ResourceListState extends State<ResourceListWidget> {
  bool showResourceDetail = false;
  ResourcesModel resourcesModel;
  ResourceData resourcesData;

  @override
  void initState() {
    super.initState();
    resourcesModel = widget.resourcesModel;
  }

  @override
  Widget build(BuildContext context) {
    if (showResourceDetail) {
      return Column(
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Flexible(
                fit: FlexFit.loose,
                flex: 0,
                child: GestureDetector(
                  onTap: () {
                    setState(() {
                      if (resourcesData.index == 0) {
                        showResourceDetail = false;
                      } else {
                        showResourceDetail = true;
                        resourcesData =
                            resourcesModel.data[resourcesData.index - 1];
                      }
                    });
                  },
                  child: Icon(
                    Icons.arrow_back_ios,
                    size: 30,
                  ),
                ),
              ),
              Expanded(
                child: GestureDetector(
                  onLongPress: () {
                    Scaffold.of(context)
                        .showSnackBar(SnackBar(content: Text('Long Press')));
                  },
                  onTap: () {
                    Scaffold.of(context)
                        .showSnackBar(SnackBar(content: Text('On Tap')));
                  },
                  onDoubleTap: () {
                    Scaffold.of(context)
                        .showSnackBar(SnackBar(content: Text('Double Tap')));
                  },
                  child: Container(
                    padding: EdgeInsets.fromLTRB(5, 40, 5, 40),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(90.0),
                      ),
                      gradient: RadialGradient(
                        colors: [
                          Colors.white,
                          _colorFromHex(resourcesData.color)
                        ],
                        tileMode: TileMode.clamp,
                      ),
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Flexible(
                          fit: FlexFit.loose,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                'Name - ${resourcesData.name}',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 17,
                                ),
                                overflow: TextOverflow.fade,
                              ),
                              Text(
                                'Year - ${resourcesData.year}',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 30,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Text(
                                'Value - ${resourcesData.pantoneValue}',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 17,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Flexible(
                fit: FlexFit.loose,
                flex: 0,
                child: GestureDetector(
                  onTap: () {
                    setState(() {
                      if (resourcesData.index ==
                          resourcesModel.data.length - 1) {
                        showResourceDetail = false;
                      } else {
                        showResourceDetail = true;
                        resourcesData =
                            resourcesModel.data[resourcesData.index + 1];
                      }
                    });
                  },
                  child: Icon(
                    Icons.arrow_forward_ios,
                    size: 30,
                  ),
                ),
              ),
            ],
          ),
          FlatButton.icon(
            textColor: _colorFromHex(resourcesData.color),
            onPressed: () {
              setState(() {
                showResourceDetail = false;
              });
            },
            icon: Icon(Icons.settings_backup_restore),
            label: Text('Back'),
          ),
        ],
      );
    } else {
      return ListBody(
        children: resourcesModel.data
            .map(
              (element) => Card(
                borderOnForeground: true,
                color: _colorFromHex(element.color),
                child: FlatButton(
                  onPressed: () {
                    setState(
                      () {
                        showResourceDetail = true;
                        resourcesData = element;
                      },
                    );
                  },
                  child: Text(
                    '${element.name} \n${element.year}',
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            )
            .toList(),
      );
    }
  }

  _colorFromHex(String hexColor) {
    hexColor = hexColor.replaceAll("#", "");
    return Color(int.parse('FF' + hexColor, radix: 16));
  }
}

class ResourceListWidget extends StatefulWidget {
  final ResourcesModel resourcesModel;

  ResourceListWidget(this.resourcesModel);

  @override
  State<StatefulWidget> createState() {
    return _ResourceListState();
  }
}
